'use strict';

var gulp 			= require('gulp');
var sass        	= require('gulp-sass');
var pug             = require('gulp-pug');
var sourcemaps 		= require('gulp-sourcemaps');
var autoprefixer 	= require('gulp-autoprefixer');
var browserSync 	= require('browser-sync').create();
var reload      	= browserSync.reload;

// Static server
gulp.task('server', ['sass', 'jade', 'copy_images', 'copy_icons'], function() {
	browserSync.init({
		reloadDelay: 500,
		notify: false,
		open: false,
		server: {
			baseDir: "./public"
		},
		ghostMode: false
	});

	gulp.watch("./sass/*.scss", ['sass']);
	gulp.watch("./jade/**/*.pug", ['jade']);
	gulp.watch("./public/*.html", reload);
	gulp.watch("./public/en/*.html", reload);
	gulp.watch("./public/ru/*.html", reload);
});

gulp.task('sass', function () {

    return gulp.src('./sass/main.scss')
    	.pipe(sourcemaps.init())
    	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('./', {
            includeContent: false,
            sourceRoot: '../sass'
        }))
        .pipe(gulp.dest('./public/css'))
        .pipe(gulp.dest('./cmf/web/assets/css'))
        .pipe(browserSync.stream());
});

gulp.task('jade', function() {
  return gulp.src('./jade/**/!(_)*.pug')
    .pipe(pug())
    .pipe(gulp.dest('./public'));
});

gulp.task('copy_images', function () {
  return gulp
    .src('img/*')
    .pipe(gulp.dest('./public/img'))
    .pipe(gulp.dest('./cmf/web/assets/images'));
});

gulp.task('copy_icons', function () {
  return gulp
    .src('icons/*')
    .pipe(gulp.dest('./public'))
    .pipe(gulp.dest('./cmf/web/assets/icons'));
});

gulp.task('build', ['sass', 'jade', 'copy_images', 'copy_icons']);

gulp.task('default', ['server']);
